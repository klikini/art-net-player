use std::net::UdpSocket;
use std::process::exit;

pub struct Controller {
    socket: UdpSocket,
    address: String,
    sequence: u8,
    brightness: f32,
    enabled: bool,
    num_channels: usize,
}

const HEADER_PREFIX: [u8; 12] = [
    b'A', b'r', b't', b'-', b'N', b'e', b't', b'\0', 0,
    0x50, // opcode
    0, 14, // version
];

impl Controller {
    pub fn new(host: &str) -> Self {
        Self {
            socket: UdpSocket::bind("0.0.0.0:0").unwrap_or_else(|error| {
                eprintln!("[Art-Net] error creating UDP socket: {}", error);
                exit(10);
            }),
            address: if host.contains(':') {
                String::from(host)
            } else {
                String::from(host) + ":6454"
            },
            sequence: 1,
            brightness: 1.0,
            enabled: true,
            num_channels: 0,
        }
    }

    pub fn send_universe(&mut self, universe: u16, values: &[u8]) {
        if !self.enabled {
            return;
        }

        self.num_channels = values.len();

        let mut buf: Vec<u8> = vec![];
        buf.extend(HEADER_PREFIX);
        buf.push(self.sequence);
        buf.push(0); // physical port
        buf.extend(universe.to_le_bytes());
        buf.extend((self.num_channels as u16).to_be_bytes());
        buf.extend(values.iter().map(|x| (*x as f32 * self.brightness) as u8));

        self.socket
            .send_to(&*buf, &self.address)
            .unwrap_or_else(|error| {
                eprintln!("[Art-Net] error sending packet {}: {}", self.sequence, error);
                0
            });

        if self.sequence == 255 {
            self.sequence = 1;
        } else {
            self.sequence += 1;
        }
    }

    pub fn send_all(&mut self, values: &[u8]) {
        let num_channels = values.len();
        let num_universes = (num_channels as f32 / 512f32).ceil() as u16;
        let mut last_sent = 0;

        for u in 0..num_universes {
            let send_channels = if u == num_universes - 1 {
                num_channels % 512
            } else {
                512
            };

            self.send_universe(u, &values[last_sent..last_sent + send_channels]);
            last_sent += send_channels;
        }
    }

    pub fn set_brightness(&mut self, brightness: u8) {
        self.brightness = brightness as f32 / 255f32;
    }

    pub fn set_state(&mut self, state: bool) {
        self.enabled = state;

        self.set_color(&if self.enabled {
            [255u8, 255u8, 255u8]
        } else {
            [0u8, 0u8, 0u8]
        });
    }

    pub fn set_color(&mut self, color: &[u8; 3]) {
        let buffer: Vec<u8> = color.iter().copied().cycle().take(self.num_channels).collect();
        self.send_all(&buffer);
    }
}
