use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;
use ffmpeg::format::{input};
use ffmpeg::frame::Video;
use ffmpeg::media::Type;
use std::process::exit;
use ffmpeg::time::sleep;

pub struct VideoPlayer {
    pub receiver: Receiver<UniverseFrame>,
    sender: Sender<UniverseFrame>,
}

pub struct UniverseFrame {
    pub data: Vec<u8>,
    pub universe: u16,
    pub done: bool,
}

impl VideoPlayer {
    pub fn new() -> Self {
        let (sender, receiver) = channel();

        Self {
            sender, receiver
        }
    }

    pub fn play(&self, file: &str) {
        let file_ = file.to_owned();
        let sender = self.sender.to_owned();

        thread::Builder::new()
            .name("Video decoder".into())
            .spawn(move || {
                let mut ctx = input(&file_).unwrap_or_else(|error| {
                    eprintln!("Error opening video '{}': {}", file_, error);
                    exit(30);
                });

                let input = ctx.streams().best(Type::Video).ok_or(ffmpeg::Error::StreamNotFound).unwrap_or_else(|error| {
                    eprintln!("Invalid video file: {}", error);
                    exit(31);
                });

                let frame_rate_f = input.avg_frame_rate();
                let frame_rate = frame_rate_f.denominator() as f32 / frame_rate_f.numerator() as f32;

                let stream_index = input.index();
                let mut decoder = input.codec().decoder().video().unwrap();

                for (stream, packet) in ctx.packets() {
                    if stream.index() == stream_index {
                        decoder.send_packet(&packet).unwrap();

                        let mut frame = Video::empty();

                        while decoder.receive_frame(&mut frame).is_ok() {
                            let data = frame.data(0);

                            for u in 0..frame.height() {
                                let start = (u * frame.width()) as usize;
                                let end = ((u + 1) * frame.width()) as usize;

                                sender.send(UniverseFrame {
                                    data: Vec::from(&data[start..end]),
                                    universe: u as u16,
                                    done: false,
                                }).unwrap();
                            }

                            sleep((frame_rate * 1000000f32) as u32).unwrap();
                        }
                    }
                }

                sender.send(UniverseFrame { data: vec![], universe: 0, done: true }).unwrap();
            }).unwrap();
    }
}
