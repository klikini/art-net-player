mod art_net;
mod mqtt;
mod video;

#[macro_use]
extern crate clap;

extern crate ffmpeg_next as ffmpeg;

use clap::{App, Arg};
use crate::video::VideoPlayer;
use crate::art_net::Controller;
use crate::mqtt::MQTT;

fn main() {
    ffmpeg::init().unwrap();

    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(
            Arg::with_name("controller")
                .short("c")
                .takes_value(true)
                .help("Controller address")
                .required(true),
        )
        .arg(
            Arg::with_name("mqtt-broker")
                .short("m")
                .takes_value(true)
                .help("MQTT broker address")
                .required(false),
        )
        .arg(
            Arg::with_name("entity-id")
                .short("i")
                .takes_value(true)
                .help("Home Assistant entity ID")
                .default_value("art_net_player"),
        )
        .arg(
            Arg::with_name("entity-name")
                .short("n")
                .takes_value(true)
                .help("Home Assistant entity name")
                .default_value("Art-Net Player"),
        )
        .arg(
            Arg::with_name("video")
                .short("v")
                .takes_value(true)
                .help("Video file to play")
                .required(true)
        )
        .get_matches();

    let controller_address = matches.value_of("controller").unwrap();
    let mut controller = Controller::new(controller_address);

    let video_player = VideoPlayer::new();

    let mut mqtt: Option<MQTT> = None;

    if let Some(broker) = matches.value_of("mqtt-broker") {
        let entity_id = matches.value_of("entity-id").unwrap();
        let entity_name = matches.value_of("entity-name").unwrap();

        if let Some(new_mqtt) = MQTT::new(broker, entity_id, entity_name) {
            mqtt = Some(new_mqtt)
        }
    }

    let video_file = matches.value_of("video").unwrap();
    video_player.play(video_file);

    loop {
        if let Some(mqtt) = &mqtt {
            if let Ok(command) = mqtt.channel.try_recv() {
                if let Some(new_state) = command.set_state {
                    controller.set_state(new_state);
                }

                if let Some(new_brightness) = command.set_brightness {
                    controller.set_brightness(new_brightness);
                }
            }
        }

        if let Ok(frame) = video_player.receiver.try_recv() {
            if frame.done {
                video_player.play(video_file);
            } else {
                controller.send_universe(frame.universe, &frame.data);
            }
        }
    }
}
