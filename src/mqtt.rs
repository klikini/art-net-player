use std::process::exit;
use serde_json::{json, Value};
use std::thread;
use std::sync::mpsc::{Receiver, channel};
use paho_mqtt::Message;

extern crate paho_mqtt as mqtt;

pub struct MQTTCommand {
    pub set_state: Option<bool>,
    pub set_brightness: Option<u8>,
}

pub struct MQTT {
    pub channel: Receiver<MQTTCommand>,
}

impl MQTT {
    pub fn new(broker: &str, entity_id: &str, entity_name: &str) -> Option<Self> {
        let mut client = mqtt::Client::new(broker).unwrap_or_else(|error| {
            eprintln!("Error creating MQTT client: {:?}", error);
            exit(20);
        });

        let topic = format!("homeassistant/light/{}", entity_id);

        if let Err(e) = client.connect(mqtt::ConnectOptionsBuilder::new()
            .will_message(mqtt::Message::new(
                String::from(&topic) + "/availability",
                "offline",
                0,
            ))
            .finalize()
        ) {
            eprintln!("[MQTT] failed to connect to broker: {:?}", e);
            return None;
        }

        let (sender, receiver) = channel();

        if let Err(e) = client.publish(mqtt::Message::new(
            String::from(&topic) + "/config",
            json!({
                    "~": &topic,
                    "unique_id": entity_id,
                    "name": entity_name,
                    "command_topic": "~/cmd",
                    "state_topic": "~/state",
                    "availability_topic": "~/availability",
                    "schema": "json",
                    "brightness": true,
                })
                .to_string(),
            0)
        ) {
            eprintln!("[MQTT] failed to configure Home Assistant: {:?}", e);
            return None;
        }

        if let Err(e) = client.publish(mqtt::Message::new(
            String::from(&topic) + "/availability", "online", 0,
        )) {
            eprintln!("[MQTT] failed to report availability: {:?}", e);
            return None;
        }

        let cmd_topic = String::from(&topic) + "/cmd";
        let incoming = client.start_consuming();

        if let Err(e) = client.subscribe(&cmd_topic, 0) {
            eprintln!("[MQTT] failed to subscribe to command topic: {:?}", e);
            return None;
        }

        thread::Builder::new()
            .name("Home Assistant MQTT Integration".into())
            .spawn(move || {
                loop {
                    match incoming.recv() {
                        Ok(msg) => {
                            let msg = msg.unwrap();

                            if msg.topic() != cmd_topic {
                                continue;
                            }

                            let json: serde_json::Result<Value> = serde_json::from_slice(msg.payload());

                            if let Ok(command) = json {
                                if let Some(state) = command.get("state") {
                                    return match state.as_str() {
                                        Some("ON") => {
                                            sender.send(MQTTCommand {
                                                set_state: Some(true),
                                                set_brightness: None,
                                            }).unwrap();

                                            client.publish(Message::new(topic + "/state", "ON", 0))
                                                .unwrap_or_else(|error| eprintln!("[MQTT] failed to report state: {:?}", error));
                                        }
                                        Some("OFF") => {
                                            sender.send(MQTTCommand {
                                                set_state: Some(false),
                                                set_brightness: None,
                                            }).unwrap();

                                            client.publish(Message::new(topic + "/state", "OFF", 0))
                                                .unwrap_or_else(|error| eprintln!("[MQTT] failed to report state: {:?}", error));
                                        }
                                        Some(x) => {
                                            eprintln!("[MQTT] received invalid state '{}' (expected 'ON' or 'OFF')", x);
                                        }
                                        None => {
                                            eprintln!("[MQTT] received null state (expected 'ON' or 'OFF')");
                                        }
                                    };
                                }

                                if let Some(b) = command.get("brightness") {
                                    return if let Some(brightness) = b.as_i64() {
                                        sender.send(MQTTCommand {
                                            set_state: None,
                                            set_brightness: Some(brightness as u8),
                                        }).unwrap();
                                    } else {
                                        eprintln!("[MQTT] received invalid brightness '{}' (expected integer 0..255)", b);
                                    };
                                }
                            } else {
                                eprintln!("[MQTT] received invalid message '{:?}'", json);
                            }
                        }
                        Err(e) => {
                            eprintln!("[MQTT] error receiving message: {:?}", e);

                            if let Err(rce) = client.reconnect() {
                                eprintln!("[MQTT] error reconnecting to broker: {:?}", rce);
                            }
                        }
                    }
                }
            }).unwrap();

        Some(Self {
            channel: receiver,
        })
    }
}
