# Art-Net Player

Streams channel data from a video file to an Art-Net controller,
and accepts on/off and brightness commands via a Home Assistant entity.

# Usage

[Download](https://gitlab.com/klikini/art-net-player/-/jobs/artifacts/master/raw/target/x86_64-unknown-linux-gnu/release/hass-artnet?job=build_linux) the latest build for Linux

```shell
# After downloading, mark it as executable:
chmod +x hass-artnet

# Minimal usage:
# Assumes the controller receiving Art-Net data is 192.168.2.240,
# and the channel video is in the current directory as channels.mov.
./hass-artnet -c 192.168.2.240 -v channels.mov

# With Home Assistant integration via MQTT:
# Assumes the MQTT broker is 192.168.2.10.
./hass-artnet -c 192.168.2.240 -v channels.mov -m 192.168.2.10

# To see all available options:
./hass-artnet --help
```

# Building

## System requirements

See: https://github.com/zmwangx/rust-ffmpeg/wiki/Notes-on-building

## Exit status

- 1X: Art-Net error
- 2X: MQTT error
- 3X: video error
